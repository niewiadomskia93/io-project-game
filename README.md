Skład grupy:
1 Jacek Myna - Developer
2 Piotr Niedziela - Developer
3 Artur Niewiadomski - Leader / Architect
4 Robert Srokosz - Developer / Architect
5 Jakub Jastrzębski - Developer

Sprawy komunkacyjne : Robert Srokosz, Jacek Myna
Moduły logiczno-decyzyjne : Artur Niewiadomski, Piotr niedziela, Jakub Jastrzębski

Wybrana metodyka - Extreme Programming

wybrane konwencje:
kodowanie - zgodne z domyslnymi ustawieniami ReSharpera:
pola prywatne zaczynają się od podkreślnika + camel case
wszystkie składowe publcizne z duzej litery + camel case
parametry metod: z małych liter + camel case
stałe: z dużej litery + camel case

testy: przykładowy kometarz przed kazdym testem, wszystkie maja taka samą strukturę
		/// <summary>
        /// <Given>empty string</Given>
        /// <When>Calculate</When>
        /// <Then>0</Then>
        /// </summary>
w kodzie testu dodatkowo opcjonalne dyrektywy preprocesora #region		


commity:
pierwszy czasownik "added/fixed/modified" + encja + dalszy komentarz


Grupa zgadza się na specyfikację udostępnioną na stronie 
https://bitbucket.org/okulwut/mini-softwareengineering-theprojectgame/downloads/TechnicalDetails.pdf
